package practice_algorithms;

import java.util.Scanner;

public class Weekly_SalaryNet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double gross_salary, net_salary, tax_rate = 0;
		int hourly_rate, hours_worked;
		String name;

		hourly_rate = 15;

		Scanner entry = new Scanner(System.in);

		System.out.println("Enter the name of employee");
		name = entry.next();

		System.out.println("Enter hours worked");
		hours_worked = entry.nextInt();

		if (hours_worked <= 35) {

			gross_salary = hours_worked * hourly_rate;
		} else {
			gross_salary = (35 * hours_worked) + ((hours_worked - 35) * 1.5 * hourly_rate);
		}

		if (gross_salary <= 1000) {
			tax_rate = 0;
		} else {
			if (gross_salary > 1000 && gross_salary <= 1400) {
				tax_rate = gross_salary * 0.25;
			}else {
				if(gross_salary > 1400) {
					tax_rate = gross_salary * 0.45;
				}
			}
		}
		
		net_salary = gross_salary -  tax_rate;
		
		System.out.println("The employee: " + name);
		System.out.println("Earn a gross salary = " + gross_salary);
		System.out.println("Less taxes = " + tax_rate);
		System.out.println("A total net salary = " + net_salary);
		
		
		entry.close();

	}

}
