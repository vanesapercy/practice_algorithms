package practice_algorithms;

import java.util.Scanner;

public class Mcdv1 {

	public static void main (String [] args) {
		int a, b, residuo, mcd = 0;
		Scanner entrada = new Scanner (System.in);

		System.out.println("Ingrese número 1");
		a = entrada.nextInt();
		System.out.println("Ingrese número 2");
		b = entrada.nextInt();
		
		while(a % b != 0) {
			residuo = a % b;
			a = b;
			b = residuo;
			mcd = b;
		}
		
		System.out.println("El máximo común divisor es = " + mcd);
		
		entrada.close();
	}

}
