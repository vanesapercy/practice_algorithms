package practice_algorithms;

public class Operaciones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		double a = 5, raiz, num1 = 5.85;
		double	b = 7, resultado;
		double r;
		
		//División
		r = a / b;
		
		//Raiz cuadrada
		raiz = Math.sqrt(9);
		
		
		//Redondeo de un número
		resultado = Math.round(num1);
		
		
		System.out.print("División = " + r + "\n");
		System.out.print("Raíz cuadrada = " +raiz + "\n");
		System.out.print("Redondeo = " +  resultado + "\n");
		
		//Casting
		double numero1 = 3.4;
		
		//Casting para convertir a entero 
		int resultado2 = (int) Math.round(numero1);
		
		System.out.print(resultado2);
	}

}
