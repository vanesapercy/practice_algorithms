package practice_algorithms;

public class MultidimensionalArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Array de 2 dimensiones
		System.out.println("Array de 3 * 2");
		int[][] arr = new int[3][2];

		arr[0][0] = 1;
		arr[0][1] = 2;
		arr[1][0] = 3;
		arr[1][1] = 4;
		arr[2][0] = 5;
		arr[2][1] = 6;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				System.out.println(" Array " + arr[i][j]);
			}

		}

		System.out.println("--------------------------------------------------------");

		int[][] MiArray = { { 1, 2 }, { 3, 4 }, { 5, 6 } };

		for (int i = 0; i < MiArray.length; i++) {
			for (int j = 0; j < MiArray[i].length; j++) {

				System.out.println(" MiArray " + MiArray[i][j]);
			}

		}

		System.out.println("--------------------------------------------------------");
		System.out.println("Array de 3 * 4");

		int[][] Numeros = { { 7, 14, 8, 3 }, { 6, 19, 7, 2 }, { 3, 13, 4, 1 } };

		for (int[] Dimension1 : Numeros) {
			for (int Dimension2 : Dimension1) {

				System.out.println(" Numeros " + Dimension2);
			}

		}

		System.out.println("--------------------------------------------------------");
		System.out.println("Array de tres dimensiones");

		int[][][] Array3D = { { { 1, 2 }, { 3, 4 } }, { { 5, 6 }, { 7, 8 } } };

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				for (int z = 0; z < 2; z++) {
					System.out.println("arr[" + i + "][" + j + "][" + z + "] = " + Array3D[i][j][z]);
				}
			}
		}
	}

}
