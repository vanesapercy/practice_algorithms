package practice_algorithms;

import java.util.Scanner;

public class Maths_Operations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num1, num2, sum, subt, mult, div;

		Scanner entry = new Scanner(System.in);

		System.out.println("Entry number 1");
		num1 = entry.nextInt();

		System.out.println("Entry number 2");
		num2 = entry.nextInt();

		sum = num1 + num2;
		subt = num1 - num2;
		mult = num1 * num2;

		if (num2 == 0) {
			System.out.println("\n" + "Division by zero not allowed");
		} else {
			div = num1 / num2;
			System.out.println("The division of " + num1 + " / " + num2 + " = " + div);
		}

		System.out.println("The sum of " + num1 + " + " + num2 + " = " + sum);
		System.out.println("The subtraction of " + num1 + " - " + num2 + " = " + subt);
		System.out.println("The multiplication of " + num1 + " * " + num2 + " = " + mult);

		entry.close();
	}

}
