package practice_algorithms;

import java.util.Scanner;



public class PesoIdeal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String Genero = "";
		Scanner entry = new Scanner(System.in);
		double Altura;
		
		
		do {

			System.out.println("Introduce tu género H (hombre) - M (Mujer)");
			Genero = entry.next();
			
			if (Genero.equalsIgnoreCase("H") == false && Genero.equalsIgnoreCase("M") == false) {
				
				System.out.println("Has introducido un género no válido");
				System.out.println("------------------------------------------");
				System.out.println("Intenta de nuevo");
				System.out.println("------------------------------------------");
				System.out.println("Introduce tu género H (hombre) - M (Mujer)");
				Genero = entry.next();
			}

		} while (Genero.equalsIgnoreCase("H") == false && Genero.equalsIgnoreCase("M") == false);

		
		System.out.println("Introduce altura en centímetros");
		Altura = entry.nextDouble();
		


		double PesoIdeal = 0;

		if (Genero.equalsIgnoreCase("H")) {

			PesoIdeal = Altura - 110;

		} else if (Genero.equalsIgnoreCase("M")) {

			PesoIdeal = Altura - 120;
		}

		System.out.println("Tu peso ideal es: " + PesoIdeal + " Kg");
		
		entry.close();
	}

}
