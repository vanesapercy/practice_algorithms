package practice_algorithms;

public class ArrayBidimensional {

	public static void main(String[] args) {

		//Declaración e inicialización del array bidimensional
		int[][] Numeros = { { 5, 7, 9 }, { 5, 56, 8 }, { 9, 5, 7 }, { 2, 56, 67 }, { 12, 34, 67 } };

		//Ciclos anidados para recorrer el array
		for (int i = 0; i < Numeros.length; i++) {
			for (int j = 0; j < Numeros[i].length; j++) {

				System.out.println(Numeros[i][j]);
			}
		}

	}

}
