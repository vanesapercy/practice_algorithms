package practice_algorithms;

import java.util.Scanner;

public class Sum_positives {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num, count_num, sum;
		double average;

		count_num = sum = 0;
		Scanner entry = new Scanner(System.in);

		do {
			
			System.out.println("Enter positive number, to finish enter zero");
			num = entry.nextInt();
			
			if(num == 0) {
				
				System.out.println("You have entered zero, end program execution");
				
			}else {
				while(num < 0) {
					System.out.println("Negative number entered, try again with a positive");
					num = entry.nextInt();
				}
			}
			
			if(num != 0) {
				count_num = count_num + 1;
				sum = sum + num;
			}

		} while (num == 0);
		
		if(count_num > 0) {
			average = sum /count_num;
			System.out.println("The result the average is = " + average);
		}
		
		entry.close();

	}
}
