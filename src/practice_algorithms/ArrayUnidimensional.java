package practice_algorithms;

public class ArrayUnidimensional {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declarando el array
		int[] MiArray = new int[15];

		// Dando valores al array
		MiArray[0] = 29;
		MiArray[1] = 29;
		MiArray[2] = 11;
		MiArray[3] = 11;
		MiArray[4] = 1;

		// Recorriendo el array e impriendo su contenido
		for (int i = 0; i < 5; i++) {
			System.out.println("Valor del índice " + i + " = " + MiArray[i]);
		}

	}

}
