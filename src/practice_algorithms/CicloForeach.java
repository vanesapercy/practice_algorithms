package practice_algorithms;

public class CicloForeach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		int[] Valores = { 1, 2, 3, 4, 5 };
		
		System.out.println("Ciclo For each sobre array de enteros");
		for (int Elemento : Valores) {

			System.out.println(Elemento);
		}

		String[] NombrePersonas = { "Miguel", "Andrés", "Guadalupe", "Verónica", "Vanesa" };

		System.out.println("Ciclo For each sobre array de Strings");
		for (String Elemento : NombrePersonas) {

			System.out.println(Elemento);
		}

	}

}
