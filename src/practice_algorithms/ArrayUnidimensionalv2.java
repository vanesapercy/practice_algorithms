package practice_algorithms;

public class ArrayUnidimensionalv2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declarando e inilizando el array en una misma línea
		int[] MiArray = { 5, 38, -15, 92, 71, 95, 65, 25, 14, 78, 11, 91 };

		for (int i = 0; i < MiArray.length; i++) {

			System.out.println("Valor del índice " + i + " = " + MiArray[i]);
		}

	}

}
