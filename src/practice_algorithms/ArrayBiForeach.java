package practice_algorithms;

public class ArrayBiForeach {

	public static void main(String[] args) {
		
		int[][] Numeros = { { 5, 7, 9 }, { 5, 56, 8 }, { 9, 5, 7 }, { 2, 56, 67 }, { 12, 34, 67 } };
		
		//Ciclo for-each
		for (int [] Dimension1 : Numeros) {
			
			for (int Dimension2 : Dimension1) {
				
				System.out.println(Dimension2 + " ");
			}
		}

	}

}
