package practice_algorithms;

/*Mostrar la tabla de multiplicar de un número
que el usuario ingrese, pero solo mostrar si el 
resultado es impar*/

import java.util.Scanner;

public class TablaMultiplicar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int Num, Result, Residuo;
		Scanner entry = new Scanner(System.in);
		
		System.out.println("Ingrese número");
		Num = entry.nextInt();
		
		Residuo = Num % 2;
		
		if(Residuo == 0) {
			
			System.out.println("No hay resultados impares que mostrar");
		} else {
			
			for(int i = 1; i < 10; i++) {
				
				Result = Num * i;
				Residuo = Result % 2;
				
				if(Residuo != 0) {
					
					System.out.println(Num + " * " + i + " = " + Result);
				}
			}
		}
		
		entry.close();
	}

}
